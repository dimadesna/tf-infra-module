# Module Changelog

## 2.0.0 - 20191206

- Simplify for TF Cloud usage [ALB, Route53 removed]

## 1.0.0 - 20190822

- Initial version
