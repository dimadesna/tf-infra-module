variable "ami" {
  description = "The AMI ID to boot."
}

// variable "domain_name" {
//   description = "The domain name associated with the zone id."
//   default     = ""
// }

// variable "hostname" {
//   description = "The public hostname of the app server."
//   default     = "app"
// }

// variable "instance_profile_name" {
//   description = "Name of the instance profile"
// }

variable "instance_type" {
  description = "The instance type for the OpenVPN server."
}

variable "key_name" {
  description = "The key name of the Key Pair to use for the instance."
}

variable "private_subnet_id" {
  description = "The Subnet ID to boot the instance into."
}

// variable "route_zone_id" {
//   description = "Route 53 Hosted Zone ID."
// }
// variable "certificate_arn" {
//   description = "Certificate"

// }

variable "tags" {
  description = "Tags to associate with the resources."
}

variable "vpc_id" {
  description = "The VPC ID."
}

variable "vpc_cidr" {
  description = "The VPC CIDR."
}
variable "ec2_name" {
}


variable "environment" {
}