# Infra module

## Description

Please note, that some variables names have nosense(real values replaced)

Module for creation infra  for App.

## Module will creates the following pieces of AWS infrastructure:

- ALB
- ALB Target group
- EC2 Instance
- Corresponding SG
- Record at Route 53

## Usage

Sample module invokation:

```
 module "app" {
  source            = "git::ssh://git@"
  vpc_id            = data.terraform_remote_state.vpc.outputs.vpc_id
  private_subnet_id = data.terraform_remote_state.vpc.outputs.private_subnets
  vpc_cidr          = data.terraform_remote_state.vpc.outputs.vpc_cidr
  ami               = "${var.app_ami}"
  instance_type     = "${var.app_instance_type}"
  key_name          = "${var.key_name}"
  domain_name       = "${var.sub_domain}"
  route_zone_id     = "${var.route_zone_id}"
  certificate_arn   = "${var.certificate_arn}"
  ec2_name          = "${var.ec2_name}"
  tags              = "${var.tags}"
  environment       = "${var.environment}"
}


```

For the available module parameters (variables) check the variables.tf file