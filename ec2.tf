resource "aws_instance" "app" {
  ami                    = var.ami
#  iam_instance_profile   = var.instance_profile_name
  instance_type          = var.instance_type
  key_name               = var.key_name
  subnet_id              = var.private_subnet_id[0]
  tags                   = merge(var.tags, var.ec2_name)
  vpc_security_group_ids = [aws_security_group.app_sg.id]

}