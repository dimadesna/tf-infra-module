resource "aws_security_group" "app_lb_sg" {
  name        = "app_lb_sg-${var.environment}"
  vpc_id      = var.vpc_id
  description = "app load balncer security group"
  tags        = var.tags
}

resource "aws_security_group_rule" "https_in" {
  cidr_blocks       = [var.vpc_cidr]
  description       = "The SSL LB port"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.app_lb_sg.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_security_group_rule" "http_lb_in" {
  cidr_blocks       = [var.vpc_cidr]
  description       = "The HTTP LB port"
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.app_lb_sg.id
  to_port           = 80
  type              = "ingress"
}

resource "aws_security_group_rule" "egress_allow_all_lb" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.app_lb_sg.id
}

resource "aws_security_group" "app_sg" {
  name        = "app_sg-${var.environment}"
  vpc_id      = var.vpc_id
  description = "app load balncer security group"
  tags        = var.tags
}

resource "aws_security_group_rule" "http_in" {
  cidr_blocks       = [var.vpc_cidr]
  description       = "The app port"
  from_port         = 8080
  protocol          = "tcp"
  security_group_id = aws_security_group.app_sg.id
  to_port           = 8080
  type              = "ingress"
}


resource "aws_security_group_rule" "egress_allow_all_app" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.app_sg.id
}