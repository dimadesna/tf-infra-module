// resource "aws_lb" "app_alb" {
//   name               = "app-project_name-alb-${var.environment}"
//   internal           = true
//   load_balancer_type = "application"
//   security_groups    = ["${aws_security_group.app_lb_sg.id}"]
//   subnets            = "${var.private_subnet_id}"
//   tags               = "${var.tags}"
// }
// resource "aws_alb_target_group" "app-project_name-alb-tg" {
//   name     = "app-project_name-alb-tg-${var.environment}"
//   port     = 8080
//   protocol = "HTTP"
//   vpc_id   = "${var.vpc_id}"
// }
// resource "aws_alb_target_group_attachment" "app-project_name-alb-tg-attach" {
//   target_group_arn = "${aws_alb_target_group.app-project_name-alb-tg.arn}"
//   target_id        = "${aws_instance.app.id}"
//   port             = 8080
// }

// resource "aws_lb_listener" "app_redicrect_https" {
//   load_balancer_arn = "${aws_lb.app_alb.arn}"
//   port              = "80"
//   protocol          = "HTTP"

//   default_action {
//     type = "redirect"

//     redirect {
//       port        = "443"
//       protocol    = "HTTPS"
//       status_code = "HTTP_301"
//     }
//   }
// }

// resource "aws_alb_listener" "aws_alb_listener_app" {
//   load_balancer_arn = "${aws_lb.app_alb.arn}"
//   port              = "443"
//   protocol          = "HTTPS"
//   certificate_arn   = "${var.certificate_arn}"
//   default_action {
//     target_group_arn = "${aws_alb_target_group.app-project_name-alb-tg.id}"
//     type             = "forward"
//   }
// }