output "instance_id" {
  description = "The ID of the instance"
  value       = "${aws_instance.app.id}"
}

// output "fqdn" {
//   description = "The FQDN of the Instance"
//   value       = "${local.fqdn}"
// }

output "private_ip" {
  description = "The Private IP of the Instance"
  value       = "${element(concat(aws_instance.app.*.private_ip, list("")), 0)}"
}

output "public_ip" {
  description = "The Public IP of the Instance"
  value       = "${element(concat(aws_instance.app.*.public_ip, list("")), 0)}"
}